package ovh.quiquelhappy.mcplugins.vanillaprotect;

import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.EntitySpawnEvent;
import org.bukkit.event.inventory.InventoryPickupItemEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import java.util.ArrayList;
import java.util.List;

import static ovh.quiquelhappy.mcplugins.vanillaprotect.Main.alivedangermobs;

public class Events implements Listener {

    @EventHandler
    public void onBlockBreak(BlockBreakEvent event){

        Material block = event.getBlock().getType();
        List<Material> blacklist = new ArrayList<Material>();

        blacklist.add(Material.END_PORTAL);
        blacklist.add(Material.END_PORTAL_FRAME);
        blacklist.add(Material.BEDROCK);

        if(blacklist.contains(block)){
            if(block==Material.BEDROCK&&event.getPlayer().getWorld().getName().contains("nether")){
                event.setCancelled(false);
                event.getPlayer().sendMessage("<PureVanilla> This exploit is allowed for gameplay improvements");

            } else {
                event.setCancelled(true);
                event.getPlayer().sendMessage("<PureVanilla> This is considered griefing. Don't. Please :D");
            }
        }

    }

    @EventHandler
    public void onItemPickUp(InventoryPickupItemEvent event){

        Material item = event.getItem().getItemStack().getType();
        List<Material> itemblacklist = new ArrayList<Material>();

        itemblacklist.add(Material.SPAWNER);
        itemblacklist.add(Material.BEDROCK);
        itemblacklist.add(Material.END_PORTAL);
        itemblacklist.add(Material.END_PORTAL_FRAME);

        if(itemblacklist.contains(item)){
            event.setCancelled(true);
        }

    }

    @EventHandler
    public void onPlayerChat(AsyncPlayerChatEvent event){
        String msg = event.getMessage();
        Player player = event.getPlayer();

        List<String> exploitlist = new ArrayList<String>();

        exploitlist.add("carpet");
        exploitlist.add("duping");
        exploitlist.add("dupe");
        exploitlist.add("rail");
        exploitlist.add("rails");
        exploitlist.add("tnt");
    }

    @EventHandler
    public void onMobSpawn(EntitySpawnEvent event){

        List<EntityType> dangermobs = new ArrayList<EntityType>();

        dangermobs.add(EntityType.WITHER);
        dangermobs.add(EntityType.ENDER_DRAGON);

        World world = event.getLocation().getWorld();

        List<Entity> entities = world.getEntities();

        for (Entity entity : entities) {
            if(dangermobs.contains(entity.getType())){
                alivedangermobs.add(entity);

                if(alivedangermobs.size()>4){
                    event.setCancelled(true);
                    for (Player player : Main.plugin.getServer().getOnlinePlayers()) {
                        player.sendMessage("<PureVanilla> A player tried to spawn a boss, but the boss count is limited to 3");
                    }
                } else {
                    event.setCancelled(true);
                    for (Player player : Main.plugin.getServer().getOnlinePlayers()) {
                        player.sendMessage("<PureVanilla> A player spawned a mob. The alive boss count is now " + alivedangermobs.size() + " (alive) out of 3");
                    }
                }

            }
        }

    }

    public void onMobDeath(EntityDeathEvent event){

        Entity entity = event.getEntity();
        if(alivedangermobs.contains(entity)){
            alivedangermobs.remove(entity);
            for (Player player : Main.plugin.getServer().getOnlinePlayers()) {
                player.sendMessage("<PureVanilla> A boss just died. Updated boss count limiter to " + alivedangermobs.size() + " (alive) out of 3");
            }
        }

    }

}
