package ovh.quiquelhappy.mcplugins.vanillaprotect;

import org.bukkit.entity.Entity;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

public class Main extends JavaPlugin {

    static List<Entity> alivedangermobs = new ArrayList<Entity>();
    static Plugin plugin = null;
    @Override
    public void onLoad() {
        getLogger().log(Level.INFO,"Vanilla Protect is now running on the background");

        plugin=this;

        getServer().getPluginManager().registerEvents(new Events(), this);

        super.onLoad();
    }

    @Override
    public void onDisable() {
        getLogger().log(Level.INFO,"Shutting down Vanilla Protect");
        super.onDisable();
    }
}
